import axios from "axios";
import React, { useEffect, useState, useRef } from "react";
import Swal from "sweetalert2";
import { FiDelete, FiEdit, FiSave } from "react-icons/fi";
import { useNavigate } from "react-router-dom";
const Mahasiswa = () => {
  const nav = useNavigate();
  //   const [mahasiswa, setMahasiswa] = useState([]);
  //   membuat 1 state yang dapat menampung sebuah object yang berisi key data untuk menampung data response dari api dan loading memilki value false
  const [mahasiswa, setMahasiswa] = useState({ data: [], loading: false });
  //   const [loading, setLoading] = useState(false);

  let nama = useRef();
  let fakultas = useRef();
  let semester = useRef();
  //   Menggunakan Aync Await

  //   const getDataMahasiswa = async () => {
  //     setLoading(true);
  //     await axios({
  //       url: "http://localhost:3000/mahasiswa",
  //       method: "GET",
  //       headers: {
  //         "Content-Type": "application/json",
  //       },
  //     })
  //       .then((res) => {
  //         Swal.fire("Berhasil!", "Data berhasil di tambahkan.", "success");
  //         if (res.status == 200) {
  //           setMahasiswa(res.data);
  //         }
  //         setLoading(false);
  //       })
  //       .catch((err) => {
  //         Swal.fire(
  //           "Peringatan!",
  //           "Terjadi kesalahan saat menambah data",
  //           "warning"
  //         );
  //         setLoading(false);
  //       });
  //   };
  const handleEdit = (id) => {
    console.log("edit", id);
    nav(`/editmahasiswa/${id}`);
  };
  // Menggunakan Promise
  const getDataMahasiswa = async () => {
    setMahasiswa((prev) => ({
      ...prev,
      data: [],
      loading: true,
    }));
    try {
      const dataMahasiswa = await axios({
        method: "GET",
        url: "http://localhost:3000/mahasiswa",
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (dataMahasiswa.status == 200) {
        let res = dataMahasiswa.data;
        setMahasiswa((prev) => ({
          ...prev,
          data: res,
          loading: false,
        }));
      }
    } catch (err) {
      setMahasiswa((prev) => ({
        ...prev,
        data: err,
        loading: false,
      }));
      Swal.fire("Error", "Terjadi kesalahan pencarian data", "error");
    }
  };
  console.log(mahasiswa);
  //parameter id untuk menangkap parameter yang ada di button, dimana penampaan untuk paramter id bebas/optional
  const handleDelete = (id) => {
    // console.log(id);
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, Delete Data!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          method: "DELETE",
          url: `http://localhost:3000/mahasiswa/${id}`,
          headers: {
            "Content-Type": "application/json",
          },
          //tergantung dari be bisa dri body atau params
          data: {},
          //   data: JSON.stringify(id._id),
          //,  atau penulisan juga bisa seperti ini booy: JSON.stringify(id.id) karena key dan value nya sama jadi id ajja seperti contoh di atas //tergantung dari backend default body
        })
          .then((res) => {
            console.log(res);
            Swal.fire(
              "Berhasil!",
              "Data kop surat berhasil di hapus.",
              "success"
            );
            getDataMahasiswa();
          })
          .catch((err) => {
            console.log(err);
            Swal.fire(
              "Peringatan!",
              "Terjadi kesalahan saat menghapus data",
              "warning"
            );
          });
      }
    });
  };
  const handleInput = async () => {
    // e.preventDefault();
    // console.log(nama.current.value);
    // console.log(fakultas.current.value);
    // console.log(semester.current.value);
    try {
      const dataSend = {
        nama: nama.current.value,
        fakultas: fakultas.current.value,
        semester: semester.current.value,
        id: Date.now(),
      };
      if (
        !nama.current.value ||
        !fakultas.current.value ||
        !semester.current.value
      ) {
        Swal.fire(
          "Peringatan!",
          "Terjadi kesalahan saat Menginput Data",
          "warning"
        );
        return;
      }
      await axios({
        url: "  http://localhost:3000/mahasiswa",
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        data: JSON.stringify(dataSend),
      });
      getDataMahasiswa();
    } catch (err) {
      console.log(err);
      Swal.fire(
        "Peringatan!",
        "Terjadi kesalahan saat menghapus data",
        "warning"
      );
    }
  };
  useEffect(() => {
    getDataMahasiswa();
  }, []);
  console.log("data");
  return (
    <>
      {mahasiswa.loading ? (
        <h1>Loading</h1>
      ) : (
        <div className="w-screen max-w-[700px] min-h-screen flex flex-col p-6 mx-auto">
          {/* <ButtonNavbar /> */}
          <h1 className="text-sky-500">List Mahasiswa</h1>
          <form className="w-full flex flex-col gap-4 mt-4">
            <div className="flex flex-col gap-2 w-full">
              <label htmlFor="title" className="text-4xl">
                Nama
              </label>
              <input
                type="text"
                id="title"
                className="h-12, px-2 bg-white w-full text-gray-500"
                ref={nama}
                // value={defaultData.title}
                // onChange={hanldeChange}
              />
            </div>
            <div className="flex flex-col gap-2 w-full">
              <label htmlFor="title" className="text-4xl">
                Fakultas
              </label>
              <input
                type="text"
                id="title"
                ref={fakultas}
                className="h-12 px-2 bg-white w-full text-gray-500"
                // value={defaultData.title}
                // onChange={hanldeChange}
              />
            </div>
            <div className="flex flex-col gap-2 w-full">
              <label htmlFor="title" className="text-4xl">
                Semester
              </label>
              <input
                type="text"
                id="title"
                ref={semester}
                className="h-12 px-2 bg-white w-full text-gray-500"
                // value={defaultData.title}
                // onChange={hanldeChange}
              />
            </div>
            <div className="flex justify-end gap-2">
              <button
                type="button"
                className="bg-orange-500 text-white flex gap-1 p-4 items-center rounded-xl"
                onClick={() => handleInput()}
              >
                <FiSave /> SIMPAN
              </button>
            </div>
          </form>
          {mahasiswa.data.map((e) => {
            return (
              <div
                key={e.id}
                className="w-full p-4 bg-white text-gray-800 my-2 rounded-lg"
              >
                {/* <div className="absolute top-2 right-12 bottom-10">
          
        </div> */}
                <button
                  className="bg-red-500 text-gray-500 rounded-lg outline-none p-2 flex justify-center items-center ring-0"
                  onClick={() => {
                    handleDelete(e.id); //e merupakan hasil dari map mahasiswa.data yang di ubah menjadi e
                  }}
                >
                  <FiDelete /> DELETE
                </button>
                <button
                  className="bg-blue-500 text-gray-500 rounded-lg outline-none p-2 flex justify-center items-center ring-0 mt-4"
                  onClick={() => {
                    handleEdit(e.id);
                  }}
                >
                  <FiEdit /> UPDATE
                </button>
                <h3 className="font-bold text-orange-500 capitalize">
                  {e.nama}
                </h3>
                <p className="my-2 font-[inherit]">{e.fakultas}</p>
                <small className="text-gray-500">{e.semester}</small>
              </div>
            );
          })}
        </div>
      )}
    </>
  );
};

export default Mahasiswa;
