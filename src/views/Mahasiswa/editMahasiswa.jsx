import axios from "axios";
import React, { useEffect, useState, useRef } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { FiSave, FiRepeat } from "react-icons/fi";
import Swal from "sweetalert2";

const Editmhs = () => {
  let { id } = useParams();
  const nav = useNavigate();
  const [update, setUpdate] = useState({
    nama: "",
    fakultas: "",
    semester: "",
  });
  let nama = useRef();
  let semester = useRef();
  let fakultas = useRef();
  const getDataMahasiswa = async () => {
    await axios({
      method: "GET",
      url: `http://localhost:3000/mahasiswa/${id}`,
      headers: {
        "Content-Type": "application/json",
      },
      body: {},
    })
      .then((res) => {
        console.log(res);
        if (res.status == 200) {
          let data = res.data;
          setUpdate({
            nama: data.nama,
            fakultas: data.fakultas,
            semester: data.semester,
          });
        }
      })
      .catch((err) => {
        console.log(err);
        nav("/addmahasiswa");
      });
  };
  // Mengunakan useRef
  const handleInputData = async () => {
    try {
      const dataSend = {
        nama: nama.current.value,
        fakultas: fakultas.current.value,
        semester: semester.current.value,
      };
      //   console.log(dataSend);
      await axios({
        method: "PUT",
        url: `http://localhost:3000/mahasiswa/${id}`,
        headers: {
          "Content-Type": "application/json",
        },
        data: JSON.stringify(dataSend),
      });
      nav("/addmahasiswa");
    } catch (err) {
      Swal.fire(
        "Peringatan!",
        "Terjadi kesalahan saat menghapus data",
        "warning"
      );
    }
  };

  //   const handleUpdate = () => {
  //     let updateData = update;
  //     console.log(updateData);
  //     Swal.fire({
  //       title: "Are you sure?",
  //       text: "You won't be able to revert this!",
  //       icon: "warning",
  //       showCancelButton: true,
  //       confirmButtonColor: "#3085d6",
  //       cancelButtonColor: "#d33",
  //       confirmButtonText: "Yes, Delete Data!",
  //     }).then((result) => {
  //       if (result.isConfirmed) {
  //         axios({
  //           method: "PUT",
  //           url: `http://localhost:3000/mahasiswa/${id}`,
  //           headers: {
  //             "Content-Type": "application/json",
  //           },
  //           //tergantung dari be bisa dri body atau params
  //           data: JSON.stringify(updateData),
  //           //   data: JSON.stringify(id._id),
  //           //,  atau penulisan juga bisa seperti ini booy: JSON.stringify(id.id) karena key dan value nya sama jadi id ajja seperti contoh di atas //tergantung dari backend default body
  //         })
  //           .then((res) => {
  //             console.log(res);
  //             Swal.fire(
  //               "Berhasil!",
  //               "Data kop surat berhasil di hapus.",
  //               "success"
  //             );
  //             nav("/addmahasiswa");
  //             // getDataMahasiswa(); untuk melakukan get ulang ketika data udah di update
  //           })
  //           .catch((err) => {
  //             console.log(err);
  //             Swal.fire(
  //               "Peringatan!",
  //               "Terjadi kesalahan saat menghapus data",
  //               "warning"
  //             );
  //             nav("/addmahasiswa");
  //           });
  //       }
  //     });
  //   };
  useEffect(() => {
    getDataMahasiswa();
  }, []);
  return (
    <div className="w-screen max-w-[700px] min-h-screen flex flex-col p-6 mx-auto">
      <h1>Edit Mahasiswa</h1>
      <small>{id}</small>
      <form className="w-full flex flex-col gap-4 mt-4">
        <div className="flex flex-col gap-2 w-full">
          <label htmlFor="nama" className="text-4xl">
            Nama Mahasiswa
          </label>
          <input
            type="text"
            id="nama"
            className="h-12 px-2 bg-white w-full text-gray-500"
            // value={update.nama} harus menggunkan funtion setUpdate
            defaultValue={update.nama}
            // menggunakan ref harus menggunakan defaultValue
            ref={nama}
            //menggunakan function setUpdate
            // onChange={(t) => {
            //   setUpdate({ ...update, nama: t.target.value });
            // }}
          />
        </div>
        <div className="flex flex-col gap-2 w-full">
          <label htmlFor="fakultas" className="text-4xl">
            Fakultas
          </label>
          <input
            type="text"
            id="fakultas"
            className="h-12 px-2 bg-white w-full text-gray-500"
            // value={update.fakultas}
            defaultValue={update.fakultas}
            ref={fakultas}
            // onChange={(t) => {
            //   setUpdate({ ...update, fakultas: t.target.value });
            // }}
          />
        </div>
        <div className="flex flex-col gap-2 w-full">
          <label htmlFor="semester" className="text-4xl">
            Semester
          </label>
          <input
            type="text"
            id="semester"
            className="h-12 px-2 bg-white w-full text-gray-500"
            // value={update.semester}
            defaultValue={update.semester}
            ref={semester}
            // onChange={(t) => {
            //   setUpdate({ ...update, semester: t.target.value });
            // }}
          />
        </div>
        <div className="flex justify-end gap-2">
          <button
            type="reset"
            className="bg-orange-500 text-white flex gap-1 p-4 items-center rounded-xl"
          >
            <FiRepeat /> CANCEL
          </button>
          <button
            type="button"
            className="bg-orange-500 text-white flex gap-1 p-4 items-center rounded-xl"
            // onClick={handleUpdate}
            // event onClick hanya menerima fungsi {()=>props.handleInputData(e.id)} tidak bisa menerima invoke {props.handleInputData(e.id)}
            onClick={handleInputData}
          >
            <FiSave /> UPDATE
          </button>
        </div>
      </form>
    </div>
  );
};

export default Editmhs;
