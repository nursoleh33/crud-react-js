import axios from "axios";
import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { FiSave, FiRepeat } from "react-icons/fi";
import { useParams } from "react-router-dom";

import { useNavigate } from "react-router-dom";
import ButtonNavbar from "../../components/ButtonNavbar";

const Edit = () => {
  let { id } = useParams();
  const nav = useNavigate();
  const [defaultData, setDefaultData] = useState({
    title: "wait",
    contet: "wait",
  });
  useEffect(() => {
    axios
      .get(`http://localhost:3000/notes/${id}`)
      .then((res) => {
        console.log(res.data);
        setDefaultData({
          title: res.data.title,
          contet: res.data.contet,
        });
      })
      .catch((err) => {
        console.log(err);
        nav("/");
      });
    return () => {};
  }, []);
  const hanldeChange = (e) => {
    setDefaultData({
      ...defaultData,
      [e.target.id]: e.target.value,
    });
    console.log(defaultData);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .put(`http://localhost:3000/notes/${id}`, {
        title: defaultData.title,
        contet: defaultData.contet,
      })
      .then((res) => {
        console.info(res.status);
        nav("/");
      })
      .catch((err) => console.error(err));
  };
  return (
    <div className="w-screen max-w-[700px] min-h-screen flex flex-col p-6 mx-auto">
      <h1>Edit Note</h1>
      <small>{id}</small>
      <ButtonNavbar />
      <form className="w-full flex flex-col gap-4 mt-4" onSubmit={handleSubmit}>
        <div className="flex flex-col gap-2 w-full">
          <label htmlFor="title" className="text-4xl">
            Title
          </label>
          <input
            type="text"
            id="title"
            className="h-12 px-2 bg-white w-full text-gray-500"
            value={defaultData.title}
            onChange={hanldeChange}
          />
        </div>
        <div className="flex flex-col gap-2 w-full">
          <label htmlFor="contet" className="text-4xl">
            Notes
          </label>
          <textarea
            name="contet"
            id="contet"
            cols="30"
            rows="10"
            className="h-[200px] px-2 bg-white w-full text-gray-500"
            value={defaultData.contet}
            onChange={hanldeChange}
          ></textarea>
        </div>
        <div className="flex justify-end gap-2">
          <button
            type="reset"
            className="bg-orange-500 text-white flex gap-1 p-4 items-center rounded-xl"
          >
            <FiRepeat /> reset
          </button>
          <button
            type="submit"
            className="bg-orange-500 text-white flex gap-1 p-4 items-center rounded-xl"
          >
            <FiSave /> submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default Edit;
