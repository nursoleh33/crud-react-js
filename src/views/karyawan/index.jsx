import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { FiDelete, FiEdit, FiSave } from "react-icons/fi";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
// import Mahasiswa from "../Mahasiswa/addMahasiswa";
axios;

function Karyawan() {
  const [karyawan, setKaryawan] = useState([]); //get
  const [dataKaryawan, setDataKaryawan] = useState({
    nama: "",
    employe: "",
    posisi: "",
    status: "",
    shows: "",
    id: "",
  }); //updae/tambah/delete
  const [mode, setMode] = useState("add");
  const [isSubmitted, setSubmitted] = useState(false);
  const [loading, setLoading] = useState(false);
  // const { id } = useParams();
  const getData = async () => {
    try {
      setLoading(true);
      const response = await axios({
        method: "GET",
        url: `http://localhost:3004/karyawans`,
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (response.status == 200) {
        let res = response.data;
        setKaryawan(res);
        // Swal.fire("Berhasil!", "Data kop surat berhasil di hapus.", "success");
        setLoading(false);
        setSubmitted(false);
      }
    } catch (err) {
      console.error(err);
      Swal.fire("Error", "Terjadi kesalahan pencarian data", "error");
      setLoading(false);
    }
  };
  console.log(karyawan);
  useEffect(() => {
    getData();
    // if (id !== undefined) {

    // }
  }, [isSubmitted]);
  const deleteData = async (id) => {
    console.log(id);
    try {
      const res = await axios({
        method: "DELETE",
        url: `http://localhost:3004/karyawans/${id}`,
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (res.status == 200) {
        Swal.fire("Sukses", "Data warga berhasil dihapus", "success");
        setSubmitted(true);
        // let res = response.data;
      }
    } catch (err) {
      // console.log(err)
      Swal.fire("Error", "Data warga gagal", "error");
    }
  };

  const submitMainData = (id) => {
    // window.scrollTo(0, 100);
    // window.scrollTo(0, 9999);
    console.log(id);
    let data = {
      ...dataKaryawan,
    };
    console.log(data);
    setLoading(true);
    console.log(id);
    let uid = id ?? "";
    // console.log(uid);
    //jika id ada / tidak sama dengan "" maka melakukan update
    if (id !== "") {
      setMode("update");
    } else {
      setMode("add");
    }
    axios({
      method: id !== "" ? "PUT" : "POST",
      url: `http://localhost:3004/karyawans/${uid}`,
      headers: {
        "Content-Type": "application/json",
      },
      data: JSON.stringify(data),
    })
      .then((res) => {
        getData(res.data.id); //akan melaukan updat elist jika ada update & add
        setLoading(false);
        setSubmitted(true);
        setMode("add");
        // if (id !== "") {
        //   setMode("add");
        // }else{
        //   setMode()
        // }
        // ketika Berhasil Input data di kosongkan
        setDataKaryawan({
          nama: "",
          employe: "",
          posisi: "",
          status: "",
          shows: "",
          id: 0,
        });
        Swal.fire("Sukses", "Data warga berhasil disimpan", "success");
        // document.getElementById("scroll").scrollTop =
        //   document.getElementById("scroll").scrollHeight;
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  };
  //Paramter karyawan dari state karyawan
  const getUpdate = (karyawan) => {
    window.scrollTo({ top: 0, behavior: "smooth" });
    // console.log("data", karyawan);
    // alert("sukses", karyawan);
    // // let data = {
    // //   nama = e.target.nama
    // // }
    let data = {
      ...dataKaryawan,
    };
    console.log(data);
    setMode("update");
    setDataKaryawan({
      nama: karyawan.nama,
      employe: karyawan.employe,
      posisi: karyawan.posisi,
      status: karyawan.status,
      shows: karyawan.shows,
      id: karyawan.id,
    });
    console.log({
      nama: karyawan?.nama,
      employe: karyawan?.employe,
      posisi: karyawan?.posisi,
      status: karyawan?.status,
      shows: karyawan.shows,
      id: karyawan?.id,
    });
    // console.log("id update", id);
  };
  let statusmahasiswa = [
    {
      label: "Aktif",
      value: "aktif",
    },
    {
      label: "Nonaktif",
      value: "nonaktif",
    },
  ];
  return (
    <>
      {loading ? (
        <h1>Loading</h1>
      ) : (
        <div className="w-screen max-w-[700px] min-h-screen flex flex-col p-6 mx-auto">
          {/* <ButtonNavbar /> */}
          <h1 className="text-sky-500">List Karyawan</h1>
          <form className="w-full flex flex-col gap-4 mt-4">
            <div className="flex flex-col gap-2 w-full">
              <label htmlFor="nama" className="text-4xl">
                Nama
              </label>
              <input
                type="text"
                id="nama"
                className="h-12 px-2 bg-white w-full text-gray-500"
                // defaultValue={karyawan.nama}
                value={dataKaryawan.nama}
                onChange={(t) => {
                  setDataKaryawan({ ...dataKaryawan, nama: t.target.value });
                }}
                // value={defaultData.title}
                // onChange={hanldeChange}
              />
            </div>
            <div className="flex flex-col gap-2 w-full">
              <label htmlFor="employe" className="text-4xl">
                Employe
              </label>
              <input
                type="text"
                id="employe"
                className="h-12 px-2 bg-white w-full text-gray-500"
                value={dataKaryawan.employe}
                onChange={(t) => {
                  setDataKaryawan({ ...dataKaryawan, employe: t.target.value });
                }}
                // value={defaultData.title}
                // onChange={hanldeChange}
              />
            </div>
            <div className="flex flex-col gap-2 w-full">
              <label htmlFor="posisi" className="text-4xl">
                Posisi
              </label>
              <input
                type="text"
                id="posisi"
                className="h-12 px-2 bg-white w-full text-gray-500"
                value={dataKaryawan.posisi}
                onChange={(t) => {
                  setDataKaryawan({ ...dataKaryawan, posisi: t.target.value });
                }}
                // value={defaultData.title}
                // onChange={hanldeChange}
              />
            </div>
            <div className="flex flex-col gap-2 w-full">
              <label htmlFor="posisi" className="text-4xl">
                Status
              </label>
              <select
                name="status"
                id="status"
                className="h-10 border-none outline-none"
                value={dataKaryawan.status}
                onChange={(e) => {
                  setDataKaryawan({ ...dataKaryawan, status: e.target.value });
                }}
              >
                {statusmahasiswa.map((val, index) => {
                  return (
                    <>
                      <option key={index} value={val.value}>
                        {val.label}
                      </option>
                    </>
                  );
                })}
              </select>
            </div>
            <div className="flex flex-col gap-2 w-full">
              <label htmlFor="apps" className="text-4xl">
                Tampilkan di Apps
              </label>
              <input
                type="checkbox"
                id="apps"
                className="h-12 px-2 bg-white w-full text-gray-500"
                checked={dataKaryawan.shows == "Y" ? true : false}
                onChange={(e) => {
                  setDataKaryawan({
                    ...dataKaryawan,
                    shows: e.target.checked ? "Y" : "N",
                  });
                }}
                // value={dataKaryawan.posisi}
                // onChange={(t) => {
                //   setDataKaryawan({ ...dataKaryawan, posisi: t.target.value });
                // }}
                // value={defaultData.title}
                // onChange={hanldeChange}
              />
            </div>
            <div className="flex justify-end gap-2">
              <button
                type="button"
                className="bg-orange-500 text-white flex gap-1 p-4 items-center rounded-xl"
                onClick={() => submitMainData(dataKaryawan.id)}
              >
                {mode === "add" ? (
                  <>
                    <FiSave /> SIMPAN
                  </>
                ) : (
                  <>
                    <FiEdit /> UPDATE
                  </>
                )}
              </button>
            </div>
          </form>
          {karyawan.map((e) => {
            // console.log(dataKaryawan);
            return (
              <div
                key={e.id}
                className="w-full p-4 bg-white text-gray-800 my-2 rounded-lg"
              >
                <button
                  className="bg-red-500 text-gray-500 rounded-lg outline-none p-2 flex justify-center items-center ring-0"
                  onClick={() => deleteData(e.id)}
                  //   onClick={() => {
                  //     handleDelete(e.id); //e merupakan hasil dari map mahasiswa.data yang di ubah menjadi e
                  //   }}
                >
                  <FiDelete /> DELETE
                </button>
                <button
                  className="bg-blue-500 text-gray-500 rounded-lg outline-none p-2 flex justify-center items-center ring-0 mt-4"
                  onClick={() => getUpdate(e)}
                >
                  <FiEdit /> UPDATE
                </button>
                <h3 className="font-bold text-orange-500 capitalize">
                  {e.nama}
                </h3>
                <p className="my-2 font-[inherit]">{e.employe}</p>
                <small className="text-gray-500">{e.posisi}</small>
                <p className="text-gray-500">{e.status}</p>
                <p className="text-gray-500">{e.shows}</p>
                {/* <div className="top" id="scroll">
                </div> */}
              </div>
            );
          })}
        </div>
      )}
    </>
  );
}

export default Karyawan;
