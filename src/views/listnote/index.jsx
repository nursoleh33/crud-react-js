import axios from "axios";
import React from "react";
import { useEffect, useRef } from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import ButtonNavbar from "../../components/ButtonNavbar";
import { FiDelete, FiEdit } from "react-icons/fi";
import { Link } from "react-router-dom";

function ListNote() {
  const linkRef = useRef(null);
  const nav = useNavigate();
  const [notes, setNotes] = useState([]);
  const [resfresh, setRefresh] = useState(false);
  // ref akan mengirimkan ke argument linkRef.current
  const goto = (ref) => {
    console.log(ref);
    window.scrollTo({
      top: ref.offsetTop,
      left: 0,
      behavior: "smooth",
    });
  };
  const handleDelete = (id) => {
    // e.preventDefault()
    console.log(id);
    axios
      .delete(`http://localhost:3000/notes/${id}`)
      .then((res) => {
        // console.info(res.status);
        setRefresh(!resfresh);
      })
      .catch((err) => {
        console.error(err);
      });
  };
  //hit api utnuk dapatkan data
  useEffect(() => {
    axios
      .get("http://localhost:3000/notes?_sort=date&_order=desc")
      .then((res) => {
        // console.info(res.data);
        setNotes(res.data);
        // setRefresh(!resfresh);
      });
    console.log(notes);
    //untuk clean
    return () => {};
  }, [resfresh]);

  const handleEdit = (id) => {
    console.info("karyawan", id);
    alert("hallo", id);
    nav(`/editnote/${id}`);
  };

  return (
    <div className="w-screen max-w-[700px] min-h-screen flex flex-col p-6 mx-auto">
      <ButtonNavbar />
      <h1 className="text-sky-500">List Note</h1>

      <div onClick={() => goto(linkRef.current)}>
        <span>Tujuan Scroll</span>
      </div>
      {notes.map((e) => {
        return (
          <div
            key={e.id}
            className="w-full p-4 bg-white text-gray-800 my-2 rounded-lg"
          >
            {/* <div className="absolute top-2 right-12 bottom-10">
              
            </div> */}
            <button
              className="bg-red-500 text-gray-500 rounded-lg outline-none p-2 flex justify-center items-center ring-0"
              onClick={() => {
                handleDelete(e.id);
              }}
            >
              <FiDelete /> Hapus
            </button>
            <button
              className="bg-blue-500 text-gray-500 rounded-lg outline-none p-2 flex justify-center items-center ring-0"
              onClick={() => {
                handleEdit(e.id);
              }}
            >
              <FiEdit /> Ubah
            </button>
            <h3 className="font-bold text-orange-500 capitalize">{e.title}</h3>
            <p className="my-2 font-[inherit]">{e.contet}</p>
            <small className="text-gray-500">{e.date}</small>
          </div>
        );
      })}
      <Link to={"/"} ref={linkRef}>
        Disini
      </Link>
    </div>
  );
}

export default ListNote;
