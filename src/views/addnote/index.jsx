import moment from "moment";
import React from "react";
import { FiSave, FiRepeat } from "react-icons/fi";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import ButtonNavbar from "../../components/ButtonNavbar";

const AddNote = () => {
  const navigate = useNavigate();
  const handleSubmit = (e) => {
    //stop form relode page
    e.preventDefault();
    // console.info(e.target.email.type);
    let title = e.target.title.value;
    let contet = e.target.contet.value;
    let date = moment().format("DD/MM/YYYY hh:mm:ss");
    let ID = Date.now();
    console.log({
      title,
      contet,
      date,
      ID,
    });
    // true and flasei !
    if (title == "" || contet == "") {
      alert("Harus di isi");
      return;
    }
    axios("http://localhost:3000/notes", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      data: {
        id: ID,
        title: title,
        contet: contet,
        date: date,
      },
    })
      .then((res) => {
        // console.log(res);
        navigate("/");
      })
      .catch((err) => {
        console.error(err);
      });
  };
  return (
    <div className="w-screen max-w-[700px] min-h-screen flex flex-col p-6 mx-auto">
      <h1>Add Note</h1>
      <ButtonNavbar />
      <form className="w-full flex flex-col gap-4 mt-4" onSubmit={handleSubmit}>
        <div className="flex flex-col gap-2 w-full">
          <label htmlFor="title" className="text-4xl">
            Title
          </label>
          <input
            type="text"
            id="title"
            className="h-12 px-2 bg-white w-full text-gray-500"
          />
        </div>
        <div className="flex flex-col gap-2 w-full">
          <label htmlFor="contet" className="text-4xl">
            Notes
          </label>
          <textarea
            name="contet"
            id="contet"
            cols="30"
            rows="10"
            className="h-[200px] px-2 bg-white w-full text-gray-500"
          ></textarea>
        </div>
        <div className="flex justify-end gap-2">
          <button
            type="reset"
            className="bg-orange-500 text-white flex gap-1 p-4 items-center rounded-xl"
          >
            <FiRepeat /> reset
          </button>
          <button
            type="submit"
            className="bg-orange-500 text-white flex gap-1 p-4 items-center rounded-xl"
          >
            <FiSave /> submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default AddNote;
