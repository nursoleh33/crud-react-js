// import { useState } from "react";
// import reactLogo from "./assets/react.svg";
import "./index.css";
import { Routes, Route } from "react-router-dom";
import AddNote from "./views/addnote";
import ListNote from "./views/listnote";
// import ButtonNavbar from "./components/ButtonNavbar";
import Edit from "./views/editnote";
import Mahasiswa from "./views/Mahasiswa/addMahasiswa";
import Editmhs from "./views/Mahasiswa/editMahasiswa";
import Karyawan from "./views/karyawan";

// import {}

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<ListNote />} />
        <Route path="/addnote" element={<AddNote />} />
        <Route path="/editnote/:id" element={<Edit />} />
        <Route path="/addmahasiswa" element={<Mahasiswa />} />
        <Route path="/editmahasiswa/:id" element={<Editmhs />} />
        <Route path="/karyawan" element={<Karyawan />} />
        <Route path="*" element={<ListNote />} />
      </Routes>
      {/* <ButtonNavbar /> */}
    </>
  );
}

export default App;
