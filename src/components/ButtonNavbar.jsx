import React from "react";
import { Link } from "react-router-dom";
import { FiPlus, FiList } from "react-icons/fi";

function ButtonNavbar() {
  return (
    <div className="fixed button-2 right-4 flex gap-2 z-50">
      <Link
        to={"/addnote"}
        className="h-12 w-20 flex justify-center items-center bg-orange-500 rounded-lg focus:bg-orange-300"
      >
        <FiPlus />
      </Link>
      <Link
        to={"/"}
        className="h-12 w-20 flex justify-center items-center bg-orange-500 rounded-lg focus:bg-orange-300"
      >
        <FiList />
      </Link>
    </div>
  );
}

export default ButtonNavbar;
