import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  // ketika di deploy untuk debungin console ga tampil ketika selesai di deploy, walaupun di project banyak console.log
  // esbuild: {
  //   drop: ["console", "debugger"],
  // },
  esbuild: {
    drop: [],
  },
});
